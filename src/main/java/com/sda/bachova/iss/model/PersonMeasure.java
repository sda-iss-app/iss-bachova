package com.sda.bachova.iss.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.security.Timestamp;
import java.util.Set;

@Entity
@Data
@Table(name = "Person_Measure")
public class PersonMeasure {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_at")
    @CreationTimestamp
    private Timestamp createdAt;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "measure_id")
    private Set<PersonInMeasurement> personInMeasurement;
}
