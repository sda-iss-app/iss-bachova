package com.sda.bachova.iss.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "person_in_measurement")
public class PersonInMeasurement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //mapování FK na další tabulky
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="measure_id", referencedColumnName="id")
    private PersonMeasure personMeasure;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="person_id", referencedColumnName="id")
    private Person person;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="craft_id", referencedColumnName="id")
    private Craft craft;
}