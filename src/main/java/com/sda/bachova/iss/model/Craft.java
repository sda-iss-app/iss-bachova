package com.sda.bachova.iss.model;
import javax.persistence.*;

import lombok.Data;

@Entity
@Data
@Table (name = "Craft")

public class Craft {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    public Craft(String craft, PersonInMeasurement personInMeasurement) {
    }
}
