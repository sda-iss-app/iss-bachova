package com.sda.bachova.iss.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.security.Timestamp;

@Entity
@Data
@Table(name = "Position_Measure")
public class PositionMeasure {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_at")
    @CreationTimestamp //vytvořím si datový typ časové razítko
    private Timestamp createdAt;

    @Column(name = "measure_time_seconds")
    private Long measureTimeInSec;

    private Float latitude;

    private Float longitude;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id", insertable = false, updatable = false)
    private IssSpeed issSpeed;
}
